# Eventopist "Cellar"

Cellar is an event store in the sense of Greg Young's "event sourcing" and CQRS concepts:

- implemented in Java 8
- persists domain events in a relational database (sample implementation: PostgreSQL via JDBC)
- projections framework to populate read models from domain events
- repository implementation to persist read models
- extensible via interfaces

## How to get an event store

```java
javax.sql.DataSource dataSource = ... /* you provide a data source */;
EventStore eventStore = EventStoreProvider.getInstance().get(dataSource);
```

## How to persist domain events in the event store

```java
Long cardId1 = eventStore.newStreamId();
GiftCard card1 = GiftCard.create(new IssueGiftCard(cardId1, "Susy's card", 10000));
assertEquals(10000, card1.remainingValue);

Long transactionId1 = eventStore.newStreamId();
card1.redeemFrom(new RedeemFromCard(cardId1, transactionId1, 6000));
assertEquals(4000, card1.remainingValue);

eventStore.saveEvents(GiftCard.class, cardId1, card1.domainEvents());
```

## How to playback domain events into an aggregate

```java
final List<Event> events1 = eventStore.getEventsFor(cardId1);
final GiftCard card1 = EventConsumer.apply(new GiftCard(), events1);
assertNotNull(card1);
assertEquals(4000, card1.remainingValue);
```

## How to playback domain events into a read model

```java
final List<Event> events1 = eventStore.getEventsFor(cardId1);
final GiftCardDetail card1detail = EventConsumer.apply(new GiftCardDetail(), events1);
```

## How to persist a read model in a read model repository

```java
// create a repository class for your read model:

public class GiftCardDetailRepo
        extends AbstractSqlBasedReadModelRepo<GiftCard, GiftCardDetail>
        implements ReadModelRepository<GiftCardDetail> {

    /**
     * @param dataSource datasource where the repo can persist its data
     */
    public GiftCardDetailRepo(DataSource dataSource) {
        super(dataSource, GiftCard.class, GiftCardDetail.class, "id");
    }
}

// ... later, the projector will automatically persist it like this:
final GiftCardDetail detail = /* get this from above... */;
giftCardDetails.transact(tx -> giftCardDetails.save(tx, detail));
```

## How to create and automatically persist a projection from GiftCard to GiftCardDetail

```java
// create a projector, using the AbstractProjector base class:

public class GiftCardDetailProjector
        extends AbstractProjector<GiftCard, GiftCardDetail>
        implements Projector<GiftCardDetail> {

    public GiftCardDetailProjector(EventStore eventStore, ReadModelRepository<GiftCardDetail> readModels) {
        super(eventStore, GiftCard.class, readModels);
    }
}

// ...and later, in your CQRS "query" handler, write this:
final ReadModelRepository<GiftCardDetail> giftCardDetails =
    new GiftCardDetailRepo(dataSource);

final GiftCardDetailProjector giftCardDetailProjector =
    new GiftCardDetailProjector(eventStore, giftCardDetails);

// the next line will create and/or update *all* your
// GiftCardDetail read models that have been changed
// by the latest domain events (no need to think about
// which ones!)
giftCardDetailProjector.updateAllReadModels(GiftCardDetail::new);

// now, you can read the updated details:
Optional<GiftCardDetail> detail = giftCardDetails.find(cardId1);

// If you want more repository operations, you can extend
// the base class and build your own finders or savers!
```

For details on how to use Cellar, please see the test cases in ```src/test/java```.
