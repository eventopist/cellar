package org.eventopist.cellar.api.eventstore;

import java.util.Collection;
import java.util.List;

/**
 * An event store in the sense of event sourcing; Look at Greg Young's
 * <a href="https://cqrs.wordpress.com/documents/building-event-storage/">article</a>
 * about this.
 */
public interface EventStore {
    /**
     * @return a new ID for a future event stream
     */
    Long newStreamId();

    /**
     * @param aggregateType the type of the aggregate that caused the events to be saved
     * @param streamId      the ID of the event stream into which the events shall be saved
     * @param events        the events to be saved
     */
    void saveEvents(Class aggregateType, Long streamId, Collection<Event> events);

    /**
     * @param streamId      ID of the event stream to be loaded
     * @param sincePosition get the events that occurred after a particular position in time
     * @return the events in that event stream
     */
    List<Event> getEventsFor(Long streamId, Long sincePosition);

    default List<Event> getEventsFor(Long streamId) {
        return getEventsFor(streamId, 0L);
    }

    /**
     * @param aggregateType type of the aggregate for which to load the events
     * @param sincePosition get the events that occurred after a particular position in time
     * @return the events for *all* instances of the aggregate type
     */
    List<Event> getEventsFor(Class aggregateType, Long sincePosition);

    default List<Event> getEventsFor(Class aggregateType) {
        return getEventsFor(aggregateType, 0L);
    }
}
