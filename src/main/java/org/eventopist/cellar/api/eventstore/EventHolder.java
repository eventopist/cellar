package org.eventopist.cellar.api.eventstore;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Holds domain events while they are produced by commands
 * but before they are saved to the event store; this class
 * is typically being used as a private member of an
 * aggregate class.
 */
public class EventHolder {

    private ArrayList<Event> eventList = new ArrayList<>();
    private Consumer<Event> eventConsumer;

    public EventHolder(Consumer<Event> eventConsumer) {
        this.eventConsumer = eventConsumer;
    }

    /**
     * @param event an event to be added to the EventHolder
     */
    public void addEvent(Event event) {
        // add to internal list
        this.eventList.add(event);

        // and make the aggregate consume it immediately
        // so that the version of the aggregate
        // is incremented correctly!
        this.eventConsumer.accept(event);
    }

    /**
     * @return the events that have been added to the EventHolder
     */
    public List<Event> getEvents() {
        return this.eventList;
    }
}
