package org.eventopist.cellar.api.eventstore;

import javax.sql.DataSource;
import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * Provides an instance of EventStore without knowing which class implements it.
 */
public class EventStoreProvider {

    /**
     * @return this provider as a singleton
     */
    public static EventStoreProvider getInstance() {
        return ourInstance;
    }

    /**
     * Gives you an event store.
     *
     * @param dataSource a data source where the event store can persist the events
     * @return an event store
     */
    public EventStore get(DataSource dataSource) {
        final Iterator<EventStoreInternal> eventStoreIterator = serviceLoader.iterator();

        if (!eventStoreIterator.hasNext()) {
            throw new RuntimeException("No implementation of " + EventStoreInternal.class.getCanonicalName() + " could be found.");
        }

        final EventStoreInternal eventStore = eventStoreIterator.next();
        eventStore.setDataSource(dataSource);

        return eventStore;
    }

    /**
     * DO NOT USE this interface although it's public; it's a hack that will soon be removed!
     */
    public static interface EventStoreInternal extends EventStore {
        void setDataSource(DataSource dataSource);
    }

    private static EventStoreProvider ourInstance = new EventStoreProvider();

    private final ServiceLoader<EventStoreInternal> serviceLoader;

    private EventStoreProvider() {
        serviceLoader = ServiceLoader.load(EventStoreInternal.class);
    }
}
