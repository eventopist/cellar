package org.eventopist.cellar.api.eventstore;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A domain event, i.e. a fact about something interesting that happened in the business domain.
 */
public class Event {

    /**
     * Which aggregate caused this event?
     */
    public Long aggregateId;

    /**
     * Which version of that aggregate caused this event?
     */
    public Long originatingVersion;

    /**
     * The position (sequenced in time) of this event in the event store
     */
    @JsonIgnore  // this field should not be serialized
    public Long position;

    public Event(Long aggregateId, Long originatingVersion) {
        this.aggregateId = aggregateId;
        this.originatingVersion = originatingVersion;
        this.position = 0L;
    }

    protected Event() {
        // for JSON de-serialization, only
    }
}
