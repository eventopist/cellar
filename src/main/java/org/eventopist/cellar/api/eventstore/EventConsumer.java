package org.eventopist.cellar.api.eventstore;

import org.eventopist.cellar.api.common.Playback;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Aggregates and Read Models are consumers of domain events (in the Event Sourcing pattern);
 * this class can receive domain events and forward them to the event handlers inside
 * a derived class.
 */
public abstract class EventConsumer {
    public Long version;
    public Long position;

    private Map<? extends Class<?>, Method> playbackMethods;

    /**
     * Collects the methods of the EventConsumer in a map
     * that is indexed by the type of the Event.
     */
    protected EventConsumer() {
        this.playbackMethods = Arrays
                .stream(this.getClass().getMethods())
                .filter(this::isPlaybackMethod)
                .collect(Collectors.toMap(m -> m.getParameterTypes()[0], Function.identity()));
    }

    /**
     * @param m a method of the event consumer class
     * @return is this a method for playing back domain events or not?
     */
    private boolean isPlaybackMethod(Method m) {
        return m.isAnnotationPresent(Playback.class) && m.getParameterCount() == 1;
    }

    /**
     * @param event an event to be played back by one playback method of the EventConsumer.
     */
    protected void applyEvent(Event event) {
        if (this.version == null || event.originatingVersion > this.version) {  // de-duplicate events
            Method playback = this.playbackMethods.get(event.getClass());
            if (playback != null) {
                try {
                    playback.invoke(this, event);
                    this.version = event.originatingVersion;
                    this.position = event.position;
                }
                catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /**
     * @param events the domain events that should be applied to this event consumer object
     */
    void applyEvents(List<Event> events) {
        events.forEach(this::applyEvent);
    }

    public static <T extends EventConsumer> T apply(T consumer, List<Event> events) {
        consumer.applyEvents(events);
        return consumer;
    }
}
