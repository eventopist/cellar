package org.eventopist.cellar.api.projections;

import org.eventopist.cellar.api.eventstore.Event;
import org.eventopist.cellar.api.eventstore.EventConsumer;
import org.eventopist.cellar.api.eventstore.EventStore;
import org.eventopist.cellar.api.readmodels.ReadModelRepository;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Base class for projectors (i.e. classes that create or update read model objects from
 * several domain events); this is a base class that can be extended by concrete (type-specific) projector
 * classes.
 *
 * @param <AggregateType> type of aggregate for which the read models should be projected
 * @param <ReadModelType> type of read models that are projected
 */
public abstract class AbstractProjector<AggregateType, ReadModelType extends EventConsumer> implements Projector<ReadModelType> {

    private final EventStore eventStore;
    private final ReadModelRepository<ReadModelType> readModels;
    private final Class<AggregateType> aggregateClass;

    public AbstractProjector(EventStore eventStore,
                             Class<AggregateType> aggregateClass,
                             ReadModelRepository<ReadModelType> readModels) {
        this.eventStore = eventStore;
        this.aggregateClass = aggregateClass;
        this.readModels = readModels;
    }

    @Override
    public void updateAllReadModels(Supplier<ReadModelType> supplier) {

        // up to which position have we already updated the read models before?
        Long position = this.readModels.findLastProcessedEventPosition();

        // get the domain events that have occurred AFTER that last processed event
        // and group them by aggregate id
        Map<Long, List<Event>> eventsPerAggregate = this.eventStore.getEventsFor(this.aggregateClass, position)
                .stream()
                .collect(Collectors.groupingBy(e -> e.aggregateId));

        // OK, now we're going to find each read model that corresponds to the
        // aggregate with a particular ID. Then, we'll apply the new domain events
        // that we found in the event store above.
        // If there is no read model yet for a particular aggregate ID, we let
        // the Supplier create a new one.
        List<ReadModelType> readModels = eventsPerAggregate.keySet().stream().map(aggregateId ->
                this.readModels.find(aggregateId)
                        .map(r -> EventConsumer.apply(r, eventsPerAggregate.get(aggregateId)))
                        .orElseGet(() -> EventConsumer.apply(supplier.get(), eventsPerAggregate.get(aggregateId)))
        ).collect(Collectors.toList());

        // OK, we've got the read models now. Let's find out the position on the timeline
        // of the last domain event that happened.
        Long maxPosition = readModels
                .stream()
                .map(rm -> rm.position)
                .reduce(0L, Math::max);

        // now, persist the new or updated read models and remember the position
        // of the last event that we have processed, in the same transaction.
        this.readModels.transact(tx -> {
            this.readModels.save(tx, readModels);
            this.readModels.saveLastProcessedEventPosition(tx, maxPosition);
        });
    }

}
