package org.eventopist.cellar.api.projections;

import org.eventopist.cellar.api.eventstore.EventConsumer;

import java.util.function.Supplier;

/**
 * Creates or updates (i.e. it "projects") several domain events into read models.
 *
 * @param <ReadModelType> Type of read model(s) to be created or updated
 */
public interface Projector<ReadModelType extends EventConsumer> {
    /**
     * Updates all read models of a particular type so that they conform
     * to the latest domain events present in the event store.
     *
     * @param supplier someone who knows how to create a new instance of &lt;ReadModel&gt;
     */
    void updateAllReadModels(Supplier<ReadModelType> supplier);
}
