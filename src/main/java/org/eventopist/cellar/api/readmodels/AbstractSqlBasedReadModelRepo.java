package org.eventopist.cellar.api.readmodels;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.mapper.reflect.FieldMapper;

import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * A generic implementation of a read model repository for a particular
 * aggregate type and read model type. It persists read model objects
 * so that they can be found by a database query.
 *
 * @param <AggregateType> the type of aggregate for which the read models are persisted
 * @param <ReadModelType> the type of read models that will be persisted
 */
public abstract class AbstractSqlBasedReadModelRepo<AggregateType, ReadModelType> implements ReadModelRepository<ReadModelType> {

    protected Jdbi jdbi;
    private String nameOfIdField;

    private final Class<AggregateType> aggregateClass;
    private final Class<ReadModelType> readModelClass;

    /**
     * @param dataSource     datasource where the repo can persist its data
     * @param aggregateClass class of the aggregate that caused the domain events that are used to populate the read model
     * @param readModelClass class of the read model itself
     * @param nameOfIdField  name of the ID field in the read model table
     */
    protected AbstractSqlBasedReadModelRepo(DataSource dataSource,
                                            Class<AggregateType> aggregateClass,
                                            Class<ReadModelType> readModelClass,
                                            String nameOfIdField) {
        this.jdbi = Jdbi.create(dataSource);
        this.aggregateClass = aggregateClass;
        this.readModelClass = readModelClass;
        this.nameOfIdField = nameOfIdField;
        jdbi.registerRowMapper(readModelClass, FieldMapper.of(readModelClass));
    }

    static class HandleHolder implements ReadModelRepository.Transaction {
        Handle h;

        HandleHolder(Handle h) {
            this.h = h;
        }
    }

    @Override
    public void transact(Consumer<Transaction> txConsumer) {
        this.jdbi.useTransaction(h -> txConsumer.accept(new HandleHolder(h)));
    }

    @Override
    public Optional<ReadModelType> find(Long id) {
        return jdbi.withHandle(handle -> {
            String select = selectQuery(readModelClass);
            return handle
                    .createQuery(select)
                    .bind(nameOfIdField, id)
                    .mapTo(readModelClass)
                    .findOne();
        });
    }

    @Override
    public void save(Transaction tx, ReadModelType readModel) {
        internalSave(((HandleHolder) tx).h, readModel);
    }

    @Override
    public void save(Transaction tx, List<ReadModelType> readModels) {
        for (ReadModelType readModel : readModels) {
            internalSave(((HandleHolder) tx).h, readModel);
        }
    }

    @Override
    public Long findLastProcessedEventPosition() {
        return jdbi.withHandle(handle -> {
            return handle
                    .createQuery("SELECT position from ProcessedEvents " +
                            "WHERE aggregateType = :aggregateType " +
                            "AND readModelType = :readModelType")
                    .bind("aggregateType", aggregateClass.getCanonicalName())
                    .bind("readModelType", readModelClass.getCanonicalName())
                    .mapTo(Long.class)
                    .findOne()
                    .orElse(0L);
        });
    }

    @Override
    public void saveLastProcessedEventPosition(Transaction tx, Long position) {
        Handle handle = ((HandleHolder) tx).h;
        int updatedRows = handle
                .createUpdate("UPDATE ProcessedEvents " +
                        "SET position = :position " +
                        "WHERE aggregateType = :aggregateType " +
                        "AND readModelType = :readModelType")
                .bind("position", position)
                .bind("aggregateType", aggregateClass.getCanonicalName())
                .bind("readModelType", readModelClass.getCanonicalName())
                .execute();
        if (updatedRows == 0) {
            handle
                    .createUpdate("INSERT INTO ProcessedEvents (position, aggregateType, readModelType) " +
                            "VALUES (:position, :aggregateType, :readModelType) ")
                    .bind("position", position)
                    .bind("aggregateType", aggregateClass.getCanonicalName())
                    .bind("readModelType", readModelClass.getCanonicalName())
                    .execute();
        }
    }

    /**
     * @param clazz a class
     * @return the names of the fields of that class
     */
    protected static List<String> fieldNames(Class<?> clazz) {
        return Arrays.stream(clazz.getFields()).map(Field::getName).collect(Collectors.toList());
    }

    /**
     * @param clazz a class
     * @return a suitable INSERT statement to persist objects of that class
     */
    protected static String insertQuery(Class<?> clazz) {
        List<String> fieldNames = fieldNames(clazz);
        return String.format("INSERT INTO %s (%s) VALUES (%s)",
                clazz.getSimpleName(),
                String.join(", ", fieldNames),
                fieldNames.stream().map(n -> ":" + n).collect(Collectors.joining(", "))
        );
    }

    /**
     * Builds an UPDATE statement to update one database record for a class.
     *
     * @param clazz a class
     * @return a suitable UPDATE query to update database rows of objects of that class
     */
    protected String updateQuery(Class<?> clazz) {
        List<String> fieldNames = fieldNames(clazz);
        return String.format("UPDATE %s SET %s WHERE %s = :%s",
                clazz.getSimpleName(),
                fieldNames.stream().map(n -> String.format("%s = :%s", n, n)).collect(Collectors.joining(", ")),
                nameOfIdField, nameOfIdField
        );
    }

    /**
     * Builds a SELECT statement to find one database record for a class.
     *
     * @param clazz              a class
     * @param selectionFieldName name of a field in the class to be used in the WHERE clause of the SQL query
     * @return a suitable SELECT statement to find one database row for an object of the given class
     */
    protected String selectQuery(Class<?> clazz, String selectionFieldName) {
        List<String> fieldNames = fieldNames(clazz);
        return String.format("SELECT %s FROM %s WHERE %s = :%s",
                String.join(", ", fieldNames),
                clazz.getSimpleName(),
                selectionFieldName, selectionFieldName
        );
    }

    /**
     * Builds a SELECT query with the ID field used in the WHERE clause.
     *
     * @param clazz a class
     * @return a suitable SELECT statement to find one database row for an object of the given class
     */
    protected String selectQuery(Class<?> clazz) {
        return selectQuery(clazz, nameOfIdField);
    }

    private void internalSave(Handle handle, ReadModelType vermietung) {
        String update = updateQuery(readModelClass);
        int updatedRows = handle
                .createUpdate(update)
                .bindFields(vermietung)
                .execute();
        if (updatedRows == 0) {
            String insert = insertQuery(readModelClass);
            handle
                    .createUpdate(insert)
                    .bindFields(vermietung)
                    .execute();
        }
    }

}
