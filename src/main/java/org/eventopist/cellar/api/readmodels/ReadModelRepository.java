package org.eventopist.cellar.api.readmodels;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * A DDD repository for read models.
 */
public interface ReadModelRepository<ReadModelType> {

    /**
     * Represents a running transaction; the transaction is started inside the
     * {@link #transact(Consumer)} method and has to be passed as a parameter to the save...() methods
     * of ReadModelRepository.
     */
    static interface Transaction {
        // this is only a Marker-Interface – no methods here!
    }

    /**
     * @param tx transaction inside which operations on this repo should be executed
     */
    void transact(Consumer<Transaction> tx);

    /**
     * Finds a particular instance of a read model
     *
     * @param id ID of that instance
     * @return that instance except when there is none with that ID
     */
    Optional<ReadModelType> find(Long id);

    /**
     * Saves a read model object in the repository
     *
     * @param tx        the transaction to be used for saving (use the transact method above to get one)
     * @param readModel the object to save
     */
    void save(Transaction tx, ReadModelType readModel);

    /**
     * Saves a whole list of read models in the repository
     *
     * @param tx         the transaction to be used for saving (use the transact method above to get one)
     * @param readModels many read models to be saved inside the same transaction
     */
    void save(Transaction tx, List<ReadModelType> readModels);

    /**
     * Finds the position of the last event that was used to update instances
     * of &lt;ReadModelType&gt;
     *
     * @return the latest position number that was used to update those instances
     */
    Long findLastProcessedEventPosition();

    /**
     * Saves the position of the last event that was used to update instances
     * of &lt;ReadModelType&gt;
     *
     * @param tx       the transaction to be used for saving (use the transact method above to get one)
     * @param position the latest position number that was used to update those instances
     */
    void saveLastProcessedEventPosition(Transaction tx, Long position);
}
