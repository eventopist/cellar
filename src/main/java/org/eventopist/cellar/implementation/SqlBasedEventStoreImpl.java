package org.eventopist.cellar.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eventopist.cellar.api.eventstore.Event;
import org.eventopist.cellar.api.eventstore.EventStoreProvider;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.mapper.reflect.FieldMapper;
import org.jdbi.v3.core.statement.PreparedBatch;
import org.jdbi.v3.core.statement.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An implementation of the EventStore interface, using SQL and a relational database.
 */
public class SqlBasedEventStoreImpl implements EventStoreProvider.EventStoreInternal {
    private final static Logger LOGGER = LoggerFactory.getLogger(SqlBasedEventStoreImpl.class);

    private Jdbi jdbi;

    private static final int AGGREGATE_ID_SEQUENCE_INCREMENT = 100;
    private static final String AGGREGATE_ID_SEQUENCE_NAME = "cellar_seq";

    private Long nextstreamId = null;
    private int streamIdsConsumed = 0;

    private ObjectMapper objectMapper = new ObjectMapper();

    public SqlBasedEventStoreImpl() {
        // to be used by Java SPI, only.
    }

    /**
     * @param dataSource a data source where the event store can persist the events
     */
    @Override
    public void setDataSource(DataSource dataSource) {
        this.jdbi = Jdbi.create(dataSource);
        jdbi.registerRowMapper(EventData.class, FieldMapper.of(EventData.class));
    }

    @Override
    public Long newStreamId() {
        if (nextstreamId == null || streamIdsConsumed >= AGGREGATE_ID_SEQUENCE_INCREMENT) {
            nextstreamId = jdbi.withHandle(handle ->
                    handle.createQuery("select nextval('" + AGGREGATE_ID_SEQUENCE_NAME + "')")
                            .mapTo(Long.class)
                            .one());
            streamIdsConsumed = 0;
        }
        this.streamIdsConsumed++;
        return this.nextstreamId++;
    }

    @Override
    public void saveEvents(Class aggregateType, Long streamId, Collection<Event> events) {
        final long DUMMY_TENANT_ID = 1L;

        jdbi.useTransaction(handle -> {
            PreparedBatch batch = handle.prepareBatch(
                    "INSERT INTO event(position, tenantId, aggregateType, streamId, originatingVersion, type, data) " +
                            "VALUES ( nextPosition(), :tenantId, :aggregateType, :streamId, :originatingVersion, :type, :data::JSON)");

            for (Event event : events) {
                try {
                    String json = objectMapper.writeValueAsString(event);
                    batch
                            .bind("tenantId", DUMMY_TENANT_ID)
                            .bind("aggregateType", aggregateType.getCanonicalName())
                            .bind("streamId", streamId)
                            .bind("originatingVersion", event.originatingVersion)
                            .bind("type", event.getClass().getCanonicalName())
                            .bind("data", json)
                            .add();

                    LOGGER.debug(
                            "write aggregate {} stream {} version {} event {} json {}",
                            aggregateType.getCanonicalName(),
                            streamId,
                            event.originatingVersion,
                            event.getClass().getCanonicalName(),
                            json
                    );
                }
                catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }

            batch.execute();
        });
    }

    /**
     * Class used to de-serialize objects from the event table inside the event store;
     * for internal use only. It has to be public because otherwise the Jackson library
     * is not able to instantiate objects of it.
     */
    public static class EventData {
        String type;
        String data;
        Long position;
    }

    @Override
    public List<Event> getEventsFor(Long streamId, Long sincePosition) {
        return jdbi.withHandle(handle -> {
            Query query = handle
                    .createQuery("SELECT type, data, position FROM event " +
                            "WHERE event.streamId = :streamId AND position > :sincePosition " +
                            "ORDER BY position")
                    .bind("streamId", streamId)
                    .bind("sincePosition", sincePosition);

            List<Event> events = collectEvents(query);

            for (Event event : events) {
                LOGGER.debug(
                        "read stream {} event {}",
                        streamId,
                        event.getClass().getCanonicalName()
                );
            }
            return events;
        });
    }

    @Override
    public List<Event> getEventsFor(Class aggregateType, Long sincePosition) {
        return jdbi.withHandle(handle -> {
            Query query = handle
                    .createQuery("SELECT type, data, position FROM event " +
                            "WHERE event.aggregateType = :aggregateType AND position > :sincePosition " +
                            "ORDER BY position")
                    .bind("aggregateType", aggregateType.getCanonicalName())
                    .bind("sincePosition", sincePosition);

            List<Event> events = collectEvents(query);

            for (Event event : events) {
                LOGGER.debug(
                        "read aggregate {} event {}",
                        aggregateType.getCanonicalName(),
                        event.getClass().getCanonicalName()
                );
            }
            return events;
        });
    }

    private List<Event> collectEvents(Query query) {
        return query.mapTo(EventData.class).map(this::toEvent).collect(Collectors.toList());
    }

    private Event toEvent(EventData ed) {
        try {
            Event event = (Event) this.objectMapper.readValue(ed.data, Class.forName(ed.type));
            event.position = ed.position;
            return event;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
