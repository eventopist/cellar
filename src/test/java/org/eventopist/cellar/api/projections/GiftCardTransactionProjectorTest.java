package org.eventopist.cellar.api.projections;

import org.eventopist.cellar.api.eventstore.EventStore;
import org.eventopist.cellar.api.eventstore.EventStoreProvider;
import org.eventopist.cellar.api.readmodels.ReadModelRepository;
import org.eventopist.cellar.implementation.DatasourceProvider;
import org.eventopist.cellar.testobjects.aggregates.GiftCard;
import org.eventopist.cellar.testobjects.commands.IssueGiftCard;
import org.eventopist.cellar.testobjects.commands.RedeemFromCard;
import org.eventopist.cellar.testobjects.projectors.GiftCardTransactionProjector;
import org.eventopist.cellar.testobjects.readmodels.GiftCardTransaction;
import org.eventopist.cellar.testobjects.repositories.GiftCardTransactionRepo;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GiftCardTransactionProjectorTest {
    private static final DataSource dataSource = DatasourceProvider.getDataSource();

    private final EventStore eventStore = EventStoreProvider.getInstance().get(dataSource);

    private final ReadModelRepository<GiftCardTransaction> giftCardTransactions =
            new GiftCardTransactionRepo(dataSource);

    private final GiftCardTransactionProjector giftCardTransactionProjector =
            new GiftCardTransactionProjector(eventStore, giftCardTransactions);

    @Test
    void shouldCreateAndUpdateReadModels() {

        Long cardId1 = eventStore.newStreamId();
        GiftCard card1 = GiftCard.create(new IssueGiftCard(cardId1, "Justin's projected card", 10000));
        Long transactionId1 = eventStore.newStreamId();
        card1.redeemFrom(new RedeemFromCard(cardId1, transactionId1, 4123));

        eventStore.saveEvents(GiftCard.class, cardId1, card1.domainEvents());

        giftCardTransactionProjector.updateAllReadModels(GiftCardTransaction::new);

        // now, the projector should have *created* a GiftCardTransaction
        // read model for the gift card above. Let's test this.

        final Integer creationTestResult = giftCardTransactions.find(transactionId1)
                .map(d -> d.amount)
                .orElse(0);

        assertEquals(4123, creationTestResult);
    }
}
