package org.eventopist.cellar.api.projections;

import org.eventopist.cellar.api.eventstore.EventConsumer;
import org.eventopist.cellar.api.eventstore.EventStore;
import org.eventopist.cellar.api.eventstore.EventStoreProvider;
import org.eventopist.cellar.api.readmodels.ReadModelRepository;
import org.eventopist.cellar.implementation.DatasourceProvider;
import org.eventopist.cellar.testobjects.aggregates.GiftCard;
import org.eventopist.cellar.testobjects.commands.IssueGiftCard;
import org.eventopist.cellar.testobjects.commands.RedeemFromCard;
import org.eventopist.cellar.testobjects.projectors.GiftCardDetailProjector;
import org.eventopist.cellar.testobjects.readmodels.GiftCardDetail;
import org.eventopist.cellar.testobjects.repositories.GiftCardDetailRepo;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertTrue;

class GiftCardDetailProjectorTest {
    private static final DataSource dataSource = DatasourceProvider.getDataSource();

    private final EventStore eventStore = EventStoreProvider.getInstance().get(dataSource);

    private final ReadModelRepository<GiftCardDetail> giftCardDetails =
            new GiftCardDetailRepo(dataSource);

    private final GiftCardDetailProjector giftCardDetailProjector =
            new GiftCardDetailProjector(eventStore, giftCardDetails);

    @Test
    void shouldCreateAndUpdateReadModels() {
        final String CARD_NAME = "Susy's projected card";

        Long cardId1 = eventStore.newStreamId();
        GiftCard card1 = GiftCard.create(new IssueGiftCard(cardId1, CARD_NAME, 10000));
        eventStore.saveEvents(GiftCard.class, cardId1, card1.domainEvents());

        giftCardDetailProjector.updateAllReadModels(GiftCardDetail::new);

        // now, the projector should have *created* a GiftCardDetail
        // read model for the gift card above. Let's test this.

        final Boolean creationTestResult = giftCardDetails.find(cardId1)
                .map(d -> CARD_NAME.equals(d.cardName))
                .orElse(false);

        assertTrue(creationTestResult);

        GiftCard copyOfCard1 = EventConsumer.apply(new GiftCard(), eventStore.getEventsFor(cardId1));
        Long transactionId1 = eventStore.newStreamId();
        copyOfCard1.redeemFrom(new RedeemFromCard(cardId1, transactionId1, 4000));
        eventStore.saveEvents(GiftCard.class, cardId1, copyOfCard1.domainEvents());

        giftCardDetailProjector.updateAllReadModels(GiftCardDetail::new);

        // now, the projector should have *updated* the GiftCardDetail
        // read model for the gift card above. Let's test this.

        final Boolean updatingTestResult = giftCardDetails.find(cardId1)
                .map(d -> 6000 == d.remainingValue)
                .orElse(false);

        assertTrue(updatingTestResult);
    }
}
