package org.eventopist.cellar.api.readmodels;

import org.eventopist.cellar.api.readmodels.ReadModelRepository;
import org.eventopist.cellar.implementation.DatasourceProvider;
import org.eventopist.cellar.testobjects.readmodels.GiftCardDetail;
import org.eventopist.cellar.testobjects.repositories.GiftCardDetailRepo;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertTrue;

class AbstractSqlBasedReadModelRepoTest {
    private static final DataSource dataSource = DatasourceProvider.getDataSource();
    private final ReadModelRepository<GiftCardDetail> giftCardDetails = new GiftCardDetailRepo(dataSource);

    @Test
    void shouldStoreAndRetrieveReadModel() {
        final long ID = 666L;
        final String CARD_NAME = "The Devil's card";
        final int REMAINING_VALUE = 999;

        final GiftCardDetail detail = new GiftCardDetail();
        detail.id = ID;
        detail.cardName = CARD_NAME;
        detail.remainingValue = REMAINING_VALUE;
        giftCardDetails.transact(tx -> giftCardDetails.save(tx, detail));

        final boolean testResult = giftCardDetails
                .find(ID)
                .map(d -> CARD_NAME.equals(d.cardName) && REMAINING_VALUE == d.remainingValue)
                .orElse(false);

        assertTrue(testResult);
    }
}
