package org.eventopist.cellar.testobjects.events;

import org.eventopist.cellar.api.eventstore.Event;

public class GiftCardIssued extends Event {
    public String cardName;
    public int amount;

    public GiftCardIssued(Long cardId, Long originatingVersion, String cardName, int amount) {
        super(cardId, originatingVersion);
        this.cardName = cardName;
        this.amount = amount;
    }

    private GiftCardIssued() {
        // for JSON persistence, only!
    }

}
