package org.eventopist.cellar.testobjects.events;

import org.eventopist.cellar.api.eventstore.Event;

public class GiftCardRedeemed extends Event {

    public Long transactionId;
    public int amount;

    public GiftCardRedeemed(Long cardId, Long originatingVersion, Long transactionId, int amount) {
        super(cardId, originatingVersion);
        this.transactionId = transactionId;
        this.amount = amount;
    }

    private GiftCardRedeemed() {
        // for JSON persistence, only!
    }
}
