package org.eventopist.cellar.testobjects.commands;

public class RedeemFromCard {

    public final Long cardId;
    public final Long transactionId;
    public final int amount;

    public RedeemFromCard(Long cardId, Long transactionId, int amount) {
        this.cardId = cardId;
        this.transactionId = transactionId;
        this.amount = amount;
    }
}
