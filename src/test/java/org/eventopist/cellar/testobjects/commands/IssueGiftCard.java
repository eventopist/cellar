package org.eventopist.cellar.testobjects.commands;

public class IssueGiftCard {

    public final Long cardId;
    public final String cardName;
    public final int amount;

    public IssueGiftCard(Long cardId, String cardName, int amount) {
        this.cardId = cardId;
        this.cardName = cardName;
        this.amount = amount;
    }
}
