package org.eventopist.cellar.testobjects.readmodels;

import org.eventopist.cellar.api.common.Playback;
import org.eventopist.cellar.api.eventstore.EventConsumer;
import org.eventopist.cellar.testobjects.events.GiftCardRedeemed;

public class GiftCardTransaction extends EventConsumer {

    public Long id;
    public int amount;

    @Playback
    public void on(GiftCardRedeemed event) {
        this.amount = event.amount;
        this.id = event.transactionId;
    }

}
