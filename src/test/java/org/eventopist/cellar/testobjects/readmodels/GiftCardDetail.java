package org.eventopist.cellar.testobjects.readmodels;

import org.eventopist.cellar.api.common.Playback;
import org.eventopist.cellar.api.eventstore.EventConsumer;
import org.eventopist.cellar.testobjects.events.GiftCardRedeemed;
import org.eventopist.cellar.testobjects.events.GiftCardIssued;

public class GiftCardDetail extends EventConsumer {

    public Long id;
    public String cardName;
    public int remainingValue;

    @Playback
    public void on(GiftCardIssued event) {
        this.id = event.aggregateId;
        this.cardName = event.cardName;
        this.remainingValue = event.amount;
    }

    @Playback
    public void on(GiftCardRedeemed event) {
        this.remainingValue -= event.amount;
    }

}
