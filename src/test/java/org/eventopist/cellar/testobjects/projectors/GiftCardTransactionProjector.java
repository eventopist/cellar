package org.eventopist.cellar.testobjects.projectors;

import org.eventopist.cellar.api.eventstore.EventStore;
import org.eventopist.cellar.api.projections.AbstractProjector;
import org.eventopist.cellar.api.projections.Projector;
import org.eventopist.cellar.api.readmodels.ReadModelRepository;
import org.eventopist.cellar.testobjects.aggregates.GiftCard;
import org.eventopist.cellar.testobjects.readmodels.GiftCardTransaction;

public class GiftCardTransactionProjector
        extends AbstractProjector<GiftCard, GiftCardTransaction>
        implements Projector<GiftCardTransaction> {

    public GiftCardTransactionProjector(EventStore eventStore, ReadModelRepository<GiftCardTransaction> readModels) {
        super(eventStore, GiftCard.class, readModels);
    }
}
