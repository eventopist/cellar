package org.eventopist.cellar.testobjects.projectors;

import org.eventopist.cellar.api.eventstore.EventStore;
import org.eventopist.cellar.api.projections.AbstractProjector;
import org.eventopist.cellar.api.projections.Projector;
import org.eventopist.cellar.api.readmodels.ReadModelRepository;
import org.eventopist.cellar.testobjects.aggregates.GiftCard;
import org.eventopist.cellar.testobjects.readmodels.GiftCardDetail;

public class GiftCardDetailProjector
        extends AbstractProjector<GiftCard, GiftCardDetail>
        implements Projector<GiftCardDetail> {

    public GiftCardDetailProjector(EventStore eventStore, ReadModelRepository<GiftCardDetail> readModels) {
        super(eventStore, GiftCard.class, readModels);
    }
}
