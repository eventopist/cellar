package org.eventopist.cellar.testobjects.aggregates;

import org.eventopist.cellar.api.common.Playback;
import org.eventopist.cellar.api.eventstore.Event;
import org.eventopist.cellar.api.eventstore.EventConsumer;
import org.eventopist.cellar.api.eventstore.EventHolder;
import org.eventopist.cellar.testobjects.events.GiftCardRedeemed;
import org.eventopist.cellar.testobjects.events.GiftCardIssued;
import org.eventopist.cellar.testobjects.commands.IssueGiftCard;
import org.eventopist.cellar.testobjects.commands.RedeemFromCard;

import java.util.List;

public class GiftCard extends EventConsumer {

    public Long id;
    public String cardName;
    public int remainingValue;

    private EventHolder eventHolder = new EventHolder(this::applyEvent);

    public static GiftCard create(IssueGiftCard command) {
        GiftCard card = new GiftCard();
        GiftCardIssued cardHasBeenIssued = new GiftCardIssued(command.cardId, 1L,
                command.cardName, command.amount);
        card.signal(cardHasBeenIssued);
        return card;
    }

    public void redeemFrom(RedeemFromCard command) {
        if (command.amount <= 0) {
            throw new IllegalArgumentException("amount <= 0");
        }
        if (command.amount > remainingValue) {
            throw new IllegalStateException("amount > remaining value");
        }
        signal(new GiftCardRedeemed(id, version + 1, command.transactionId, command.amount));
    }

    public List<Event> domainEvents() {
        return eventHolder.getEvents();
    }

    @Playback
    public void on(GiftCardIssued event) {
        this.id = event.aggregateId;
        this.cardName = event.cardName;
        this.remainingValue = event.amount;
    }

    @Playback
    public void on(GiftCardRedeemed event) {
        this.remainingValue -= event.amount;
    }

    private void signal(Event event) {
        eventHolder.addEvent(event);
    }

}
