package org.eventopist.cellar.testobjects.repositories;

import org.eventopist.cellar.api.readmodels.ReadModelRepository;
import org.eventopist.cellar.api.readmodels.AbstractSqlBasedReadModelRepo;
import org.eventopist.cellar.testobjects.aggregates.GiftCard;
import org.eventopist.cellar.testobjects.readmodels.GiftCardTransaction;

import javax.sql.DataSource;

public class GiftCardTransactionRepo
        extends AbstractSqlBasedReadModelRepo<GiftCard, GiftCardTransaction>
        implements ReadModelRepository<GiftCardTransaction> {

    /**
     * @param dataSource datasource where the repo can persist its data
     */
    public GiftCardTransactionRepo(DataSource dataSource) {
        super(dataSource, GiftCard.class, GiftCardTransaction.class, "id");
    }
}
