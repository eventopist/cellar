package org.eventopist.cellar.testobjects.repositories;

import org.eventopist.cellar.api.readmodels.ReadModelRepository;
import org.eventopist.cellar.api.readmodels.AbstractSqlBasedReadModelRepo;
import org.eventopist.cellar.testobjects.aggregates.GiftCard;
import org.eventopist.cellar.testobjects.readmodels.GiftCardDetail;

import javax.sql.DataSource;

public class GiftCardDetailRepo
        extends AbstractSqlBasedReadModelRepo<GiftCard, GiftCardDetail>
        implements ReadModelRepository<GiftCardDetail> {

    /**
     * @param dataSource datasource where the repo can persist its data
     */
    public GiftCardDetailRepo(DataSource dataSource) {
        super(dataSource, GiftCard.class, GiftCardDetail.class, "id");
    }
}
