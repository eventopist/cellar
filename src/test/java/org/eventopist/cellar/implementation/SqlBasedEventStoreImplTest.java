package org.eventopist.cellar.implementation;

import org.eventopist.cellar.api.eventstore.Event;
import org.eventopist.cellar.api.eventstore.EventConsumer;
import org.eventopist.cellar.api.eventstore.EventStore;
import org.eventopist.cellar.api.eventstore.EventStoreProvider;
import org.eventopist.cellar.testobjects.aggregates.GiftCard;
import org.eventopist.cellar.testobjects.commands.IssueGiftCard;
import org.eventopist.cellar.testobjects.commands.RedeemFromCard;
import org.eventopist.cellar.testobjects.readmodels.GiftCardDetail;
import org.eventopist.cellar.testobjects.readmodels.GiftCardTransaction;
import org.junit.jupiter.api.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SqlBasedEventStoreImplTest {

    private static final DataSource dataSource = DatasourceProvider.getDataSource();
    private final EventStore eventStore = EventStoreProvider.getInstance().get(dataSource);

    @BeforeAll
    static void initDatabaseSchema() throws Exception {
        String sqlInitScript = convertStreamToString(
                SqlBasedEventStoreImplTest.class.getResourceAsStream("/giftcards-db-init.sql")
        );
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.execute(sqlInitScript);
            }
        }
    }

    private static String convertStreamToString(java.io.InputStream is) {
        try (java.util.Scanner s = new java.util.Scanner(is)) {
            return s.useDelimiter("\\A").hasNext() ? s.next() : "";
        }
    }

    @Test
    @Order(1)
    void shouldStoreEvents() {
        Long cardId1 = eventStore.newStreamId();
        GiftCard card1 = GiftCard.create(new IssueGiftCard(cardId1, "Susy's card", 10000));
        assertEquals(10000, card1.remainingValue);

        Long transactionId1 = eventStore.newStreamId();
        card1.redeemFrom(new RedeemFromCard(cardId1, transactionId1, 6000));
        assertEquals(4000, card1.remainingValue);

        eventStore.saveEvents(GiftCard.class, cardId1, card1.domainEvents());

        Long cardId2 = eventStore.newStreamId();
        GiftCard card2 = GiftCard.create(new IssueGiftCard(cardId2, "Justin's card", 4000));
        assertEquals(4000, card2.remainingValue);

        Long transactionId2 = eventStore.newStreamId();
        card2.redeemFrom(new RedeemFromCard(cardId2, transactionId2, 3000));
        assertEquals(1000, card2.remainingValue);

        eventStore.saveEvents(GiftCard.class, cardId2, card2.domainEvents());
    }

    @Test
    @Order(2)
    void shouldGetEventsForAggregateId() {
        final List<Event> events1 = eventStore.getEventsFor(1L);  // assumed ID of the GiftCard created above
        final GiftCard card1 = EventConsumer.apply(new GiftCard(), events1);
        assertNotNull(card1);
        assertEquals(4000, card1.remainingValue);

        final List<Event> events2 = eventStore.getEventsFor(3L);  // assumed ID of the GiftCard created above
        final GiftCard card2 = EventConsumer.apply(new GiftCard(), events2);
        assertNotNull(card2);
        assertEquals(1000, card2.remainingValue);
    }

    @Test
    @Order(3)
    void shouldGetEventsForAggregateType() {
        final Map<Long, List<Event>> eventListMap =
                eventStore.getEventsFor(GiftCard.class)
                        .stream()
                        .collect(Collectors.groupingBy(e -> e.aggregateId));

        final List<GiftCard> giftCards = eventListMap.values()
                .stream()
                .map(el -> EventConsumer.apply(new GiftCard(), el))
                .collect(Collectors.toList());

        for (GiftCard card : giftCards) {
            assertTrue(
                    card.id == 1L && card.remainingValue == 4000
                            || card.id == 3L && card.remainingValue == 1000
            );
        }
    }

    @Test
    @Order(4)
    void shouldGetGiftCardDetails() {
        final Map<Long, List<Event>> eventListMap =
                eventStore.getEventsFor(GiftCard.class)
                        .stream()
                        .collect(Collectors.groupingBy(e -> e.aggregateId));

        final List<GiftCardDetail> giftCards = eventListMap.values()
                .stream()
                .map(el -> EventConsumer.apply(new GiftCardDetail(), el))
                .collect(Collectors.toList());

        for (GiftCardDetail giftCardDetail : giftCards) {
            assertTrue(
                    giftCardDetail.id == 1L && giftCardDetail.remainingValue == 4000
                            || giftCardDetail.id == 3L && giftCardDetail.remainingValue == 1000
            );
        }
    }

    @Test
    @Order(5)
    void shouldGetGiftCardTransactions() {
        final Map<Long, List<Event>> eventListMap =
                eventStore.getEventsFor(GiftCard.class)
                        .stream()
                        .collect(Collectors.groupingBy(e -> e.aggregateId));

        final List<GiftCardTransaction> giftCardTransactions = eventListMap.values()
                .stream()
                .map(el -> EventConsumer.apply(new GiftCardTransaction(), el))
                .collect(Collectors.toList());

        for (GiftCardTransaction giftCardDetail : giftCardTransactions) {
            assertTrue(
                    giftCardDetail.id == 2L && giftCardDetail.amount == 6000
                            || giftCardDetail.id == 4L && giftCardDetail.amount == 3000
            );
        }
    }

}
