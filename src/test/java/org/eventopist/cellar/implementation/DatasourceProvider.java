package org.eventopist.cellar.implementation;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DatasourceProvider {

    public static HikariDataSource getDataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost/giftcards?user=fred&password=secret");

        return new HikariDataSource(config);
    }
}
