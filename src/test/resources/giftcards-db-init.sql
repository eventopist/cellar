DROP SEQUENCE IF EXISTS cellar_seq;
CREATE SEQUENCE cellar_seq START WITH 1 INCREMENT BY 100;

--Read models

DROP TABLE IF EXISTS giftcarddetail;
CREATE TABLE giftcarddetail (id BIGINT, cardname TEXT, remainingvalue BIGINT, version BIGINT, position BIGINT);

DROP TABLE IF EXISTS giftcardtransaction;
CREATE TABLE giftcardtransaction (id BIGINT, amount BIGINT, version BIGINT, position BIGINT);

DROP TABLE IF EXISTS processedevents;
CREATE TABLE processedevents (aggregatetype TEXT, readmodeltype TEXT, position BIGINT);

--From an article by Kasey Speakman, see https://dev.to/kspeakman/comment/8bdc

DROP TABLE IF EXISTS PositionCounter CASCADE;
DROP FUNCTION IF EXISTS NextPosition();
DROP TABLE IF EXISTS Event CASCADE;
DROP TRIGGER IF EXISTS trg_EventRecorded ON Event;
DROP FUNCTION IF EXISTS NotifyEvent();

-- transactional sequence number
CREATE TABLE IF NOT EXISTS PositionCounter
(
    position bigint NOT NULL
);

INSERT INTO PositionCounter VALUES (0);

-- prevent removal / additional rows

CREATE RULE rule_positioncounter_noinsert AS
ON INSERT TO PositionCounter DO INSTEAD NOTHING;

CREATE RULE rule_positioncounter_nodelete AS
ON DELETE TO PositionCounter DO INSTEAD NOTHING;

-- function to get next sequence number
CREATE FUNCTION NextPosition() RETURNS bigint AS $$
    DECLARE
        nextPos bigint;
    BEGIN
        UPDATE PositionCounter
           SET position = position + 1
        ;
        SELECT INTO nextPos Position FROM PositionCounter;
        RETURN nextPos;
    END;
$$ LANGUAGE plpgsql;

-- event table
CREATE TABLE IF NOT EXISTS Event
(
    position bigint NOT NULL,
    tenantId bigint NOT NULL,
    streamId bigint NOT NULL,
    aggregateType text NOT NULL,
    originatingVersion bigint NOT NULL,
    type text NOT NULL,
    data jsonb,
    logDate timestamptz NOT NULL DEFAULT now(),
    CONSTRAINT pk_event_position PRIMARY KEY (tenantId, position),
    CONSTRAINT uk_event_streamid_version UNIQUE (tenantId, streamId, originatingVersion)
) PARTITION BY LIST (tenantId);

-- Create several tables, one for each list of tenants
CREATE TABLE Events_for_Tenant_1 PARTITION OF Event FOR VALUES IN (1);

-- Append only
CREATE RULE rule_event_nodelete AS
ON DELETE TO Event DO INSTEAD NOTHING;

CREATE RULE rule_event_noupdate AS
ON UPDATE TO Event DO INSTEAD NOTHING;

-- notification
CREATE FUNCTION NotifyEvent() RETURNS trigger AS $$

    DECLARE
        payload text;

    BEGIN
        -- { position }/{ tenantId }/{ streamId }/{ aggregateType }/{ originatingVersion }/{ event type }
        SELECT CONCAT_WS( '/'
                        , NEW.position
                        , NEW.tenantId
                        , NEW.streamId
                        , NEW.aggregateType
                        , NEW.originatingVersion
                        , NEW.type
                        )
          INTO payload
        ;

        -- using lower case channel name or else LISTEN would require quoted identifier.
        PERFORM pg_notify('eventrecorded', payload);

        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_EventRecorded
    AFTER INSERT ON Event
    FOR EACH ROW
    EXECUTE PROCEDURE NotifyEvent()
;
